#include <iostream>
#include "lister.hpp"

using namespace std ;

int main(int argc, char **argv) {
    if (argc < 2) {
        cout << "usage: hello file1 file2 ..." << endl ;
        cout << "Version 1 " << __TIMESTAMP__ << endl ;
    }
    for (int i=1; i<argc; i++) {
        list_file( argv [i] );
    }
}
