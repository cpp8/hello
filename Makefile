CXX=clang
EXEC := hello
SOURCES = $(wildcard *.cpp)
OBJECTS = $(SOURCES:.cpp=.o)
# CXXFLAGS := -std=gnu++17 -static-libstdc++ -lstdc++fs
CXXFLAGS := -std=gnu++17
LDFLAGS := -lstdc++ -lstdc++fs
all: $(OBJECTS)
	$(CXX) $(OBJECTS) $(CXXFLAGS) $(LDFLAGS) -o $(EXEC)

clean:
	ls $(SOURCES)
	ls $(OBJECTS)
	rm -f $(EXEC) $(OBJECTS)

%.o:%.cpp
	$(CXX) $(CXXFLAGS) -c $(<)