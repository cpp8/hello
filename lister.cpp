#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
#include <filesystem>

using namespace std ;

void list_file(char *filename) {
    ifstream ifile(filename, ios::binary) ;
    if (!ifile.is_open()) {
        cout << "Error opening " << filename << endl ;
        return ;
    }

    string dashes = "============================================================" ;
    cout << endl << dashes << endl ;
    cout << std::filesystem::absolute(filename) << endl ;
    cout << dashes << endl ;
    cout << endl ;

    int linenum = 0 ;
    string iline ;

    while (!ifile.eof()) {
        
        getline(ifile,iline);
        cout << setw(4) << setfill('0') << ++linenum << " : " ;
        cout << iline ;
        cout << endl ;
    }
    ifile.close() ;
}